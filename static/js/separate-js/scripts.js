//'use strict';

$(function() {

    /*
    |--------------------------------------------------------------------------
    | Search
    |--------------------------------------------------------------------------
    */

    $('.js-search-toggle').click(function() {
        $('.search-toggle').addClass('--focused');
    });

    var search = document.querySelectorAll('.search-toggle');

    search.forEach(function(s,i){
        var flagHidden = true;
        var searchButton = s.querySelector('.js-search-toggle');
        var searchInput = s.querySelector('.search-toggle__input');

        ['click','ontouchstart'].forEach(function(evt){
            document.addEventListener(evt,function(){
                if(flagHidden){s.classList.remove('--focused')};
                flagHidden = true;
            });
        });
        ['click','ontouchstart'].forEach(function(evt){
            searchButton.addEventListener(evt,function(){
                s.classList.add('--focused');
                searchInput.focus();
                flagHidden = false;
            });
        });
        ['click','ontouchstart'].forEach(function(evt){
            searchInput.addEventListener(evt,function(){
                flagHidden = false;
            });
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Search Modal
    |--------------------------------------------------------------------------
    */

    var searchModal = $('.js-modal-search-toggle');

    // Show Search Modal
    searchModal.on('click', function(e) {

        // Toggle Open Class
        $(this).toggleClass('is-open');

        e.preventDefault();

        $('body').toggleClass('search-open');

    });

    // close search modal when click outside on mobile/table
    $(document).on('click touchstart', function(e){
        e.stopPropagation();

        // closing of search modal when clicking outside of it
        if (!$(e.target).closest(searchModal).length) {
            var sidebar = $(e.target).closest('.search-modal').length;
            var sidebarBody = $(e.target).closest('.search-modal__body').length;
            if (!sidebar && !sidebarBody) {
                if ($('body').hasClass('search-open')) {
                    $('body').removeClass('search-open');
                }
            }
        }
    });

    var searchModalClose = $('.js-search-close');

    // Sidebar toggle to sidebar-folded
    searchModalClose.on('click', function(e) {

        // Toggle Open Class
        searchModal.toggleClass('is-open');

        e.preventDefault();

        $('body').toggleClass('search-open');

    });

    /*
    |--------------------------------------------------------------------------
    | Products Toggle
    |--------------------------------------------------------------------------
    */

    $(".products__front").click(function(){
        divId = $(this).attr("title");
        $(".products__overlay").each(function(){
            if ($(this) == $("#"+divId)){
                $(this).css('display', 'flex');
            }
            else{
                $(this).css('display', 'none');
            }
        });
        $("#"+divId).css('display', 'flex');
    });

    /*
    |--------------------------------------------------------------------------
    | Image Blurry Shadow
    |--------------------------------------------------------------------------
    */

    (function(){
        var imgs = document.getElementsByClassName("ios-shadow"),
            id = 0;
        var style = "";

        for (var i = 0, j = imgs.length; i < j; ++i) {
            var img = imgs[i],
                src = img.src;

            var container = document.createElement("div");
            container.className = "ios-shadow-container ios-shadow";
            container.id = "ios-shadow-"+(++id);
            img.classList.remove("ios-shadow");

            img.parentNode.insertBefore(container, img);
            container.appendChild(img);

            style += "#ios-shadow-"+id+"::after {"+
                "  background-image: url('"+src+"');"+
                "  content: '';"+
                "}";
        }

        var $style = document.createElement("style");
        $style.appendChild(document.createTextNode(style));
        document.head.appendChild($style);
    })();

    /*
    |--------------------------------------------------------------------------
    | Sticky Kit
    |--------------------------------------------------------------------------
    */

    if(window.matchMedia('(min-width: 992px)').matches) {
        $(".js-sticky").stick_in_parent({
            recalc_every: 1,
            offset_top: 50,
        });
    }

    if(window.matchMedia('(min-width: 992px)').matches) {
        $(".js-sticky-social").stick_in_parent({
            recalc_every: 1,
            offset_top: 90,
        });
    }

    /*
    |--------------------------------------------------------------------------
    | // https://www.coralnodes.com/hide-header-on-scroll-down/
    |--------------------------------------------------------------------------
    */

    if ($('#header').length > 0) {

        var doc = document.documentElement;
        var w = window;

        var prevScroll = w.scrollY || doc.scrollTop;
        var curScroll;
        var direction = 0;
        var prevDirection = 0;
        var headerHeight = $("#header").height();

        var header = document.getElementById('header');

        var checkScroll = function() {

            /*
            ** Find the direction of scroll
            ** 0 - initial, 1 - up, 2 - down
            */

            curScroll = w.scrollY || doc.scrollTop;
            if (curScroll > prevScroll) {
                //scrolled up
                direction = 2;
            }
            else if (curScroll < prevScroll) {
                //scrolled down
                direction = 1;
            }

            if (direction !== prevDirection) {
                toggleHeader(direction, curScroll);
            }

            prevScroll = curScroll;
        };

        var toggleHeader = function(direction, curScroll) {
            if (direction === 2 && curScroll > headerHeight) {
                header.classList.add('--hidden');
                prevDirection = direction;
            }
            else if (direction === 1) {
                header.classList.remove('--hidden');
                prevDirection = direction;
            }
        };

        window.addEventListener('scroll', checkScroll);

    }

   /*
   |--------------------------------------------------------------------------
   | Showing modal with effect
   |--------------------------------------------------------------------------
   */

    $('.js-modal-effect').on('click', function(e) {

        e.preventDefault();

        var effect = $(this).attr('data-effect');
        $('.modal').addClass(effect);
    });
    // hide modal with effect

    /*
    |--------------------------------------------------------------------------
    | Mobile menu
    |--------------------------------------------------------------------------
    */

    // Click on burger button
    $('.js-menu-toggle').click(function() {

        // Toggle Open Class
        $(this).toggleClass('--open');

        // Toggle Mobile Menu
        $('.mob-menu').toggleClass('--show');

        // Disable Scroll On Body
        if ($(this).hasClass('--open')) {
            $('body').css({"overflow": "hidden"});
        } else {
            $('body').css({"overflow": ""});
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Responsive Iframe Inside Modal
    |--------------------------------------------------------------------------
    */

    function toggle_video_modal() {

        // Click on video thumbnail or link
        $(".js-video-modal").on("click", function(e){

            // prevent default behavior for a-tags, button tags, etc.
            e.preventDefault();

            // Grab the video ID from the element clicked
            var id = $(this).attr('data-youtube-id');

            // Autoplay when the modal appears
            // Note: this is intetnionally disabled on most mobile devices
            // If critical on mobile, then some alternate method is needed
            var autoplay = '?autoplay=1';

            // Don't show the 'Related Videos' view when the video ends
            var related_no = '&rel=0';

            // String the ID and param variables together
            var src = '//www.youtube.com/embed/'+id+autoplay+related_no;

            // Pass the YouTube video ID into the iframe template...
            // Set the source on the iframe to match the video ID
            $(".video-modal__iframe").attr('src', src);

            // Add class to the body to visually reveal the modal
            $("body").addClass("video-modal-show");

            $('body').css({"overflow": "hidden"});

        });

        // Close and Reset the Video Modal
        function close_video_modal() {

            event.preventDefault();

            // re-hide the video modal
            $("body").removeClass("video-modal-show");

            $('body').css({"overflow": ""});

            // reset the source attribute for the iframe template, kills the video
            $(".video-modal__iframe").attr('src', '');

        }
        // if the 'close' button/element, or the overlay are clicked
        $('body').on('click', '.video-modal__close, .video-modal__overlay', function(event) {

            // call the close and reset function
            close_video_modal();

        });
        // if the ESC key is tapped
        $('body').keyup(function(e) {
            // ESC key maps to keycode `27`
            if (e.keyCode == 27) {

                // call the close and reset function
                close_video_modal();

            }
        });
    }
    toggle_video_modal();

    /*
    |--------------------------------------------------------------------------
    | Smooth Scroll
    |--------------------------------------------------------------------------
    */

    $('.js-page-scroll').on('click', function(event) {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
            let target = $(this.hash),
                speed = $(this).data("speed") || 800;
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top - 0
                }, speed);
            }
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Spoiler Text
    |--------------------------------------------------------------------------
    */

    var containerHeight = document.querySelectorAll(".js-spoiler-inner");
    var uncoverLink = document.querySelectorAll(".js-spoiler-more");

    for(let i = 0; i < containerHeight.length; i++){
        let openData = uncoverLink[i].dataset.open;
        let closeData = uncoverLink[i].dataset.close;
        let curHeight = containerHeight[i].dataset.height;

        uncoverLink[i].innerHTML = openData;
        containerHeight[i].style.maxHeight = curHeight + "px";

        uncoverLink[i].addEventListener("click", function(){
            if(containerHeight[i].classList.contains("--open")){

                containerHeight[i].classList.remove("--open");

                uncoverLink[i].innerHTML = openData;

                containerHeight[i].style.maxHeight = curHeight + "px";

            } else {
                containerHeight[i].removeAttribute("style");

                containerHeight[i].classList.add("--open");

                uncoverLink[i].innerHTML = closeData;

            }
        });
    }

    /*
    |--------------------------------------------------------------------------
    | News Slider
    |--------------------------------------------------------------------------
    */

	let newsSlider = new Swiper('.js-news-slider', {
		speed: 600,
		mousewheel: false,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
		spaceBetween: 0,
		navigation: {
			nextEl: '.js-news-slider-next',
			prevEl: '.js-news-slider-prev',
		},
        pagination: {
            el: '.js-news-slider-pagination',
            clickable: true,
        },
		slidesPerView: 'auto',
        breakpoints: {
            // when window width is >= 320px
            320: {
                spaceBetween: 16,
                freeMode: false,
            },
            // when window width is >= 480px
            480: {
                spaceBetween: 16,
                freeMode: false,
            },
            // when window width is >= 640px
            640: {
                spaceBetween: 16,
                freeMode: false,
            }
        }
	});

    /*
    |--------------------------------------------------------------------------
    | Article Slider
    |--------------------------------------------------------------------------
    */

    let articleSlider = new Swiper('.js-article-slider', {
        speed: 600,
        mousewheel: false,
        freeMode: false,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        spaceBetween: 0,
        navigation: {
            nextEl: '.js-article-slider-next',
            prevEl: '.js-article-slider-prev',
        },
        pagination: {
            el: '.js-article-slider-pagination',
            clickable: true,
        },
        slidesPerView: 4,
        breakpoints: {
            // when window width is >= 320px
            320: {
                spaceBetween: 16,
                freeMode: false,
            },
            // when window width is >= 480px
            480: {
                spaceBetween: 16,
                freeMode: false,
            },
            // when window width is >= 640px
            640: {
                spaceBetween: 16,
                freeMode: false,
            }
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Products Slider
    |--------------------------------------------------------------------------
    */

    let productsSlider = new Swiper('.js-products-slider', {
        speed: 600,
        mousewheel: false,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        spaceBetween: 0,
        navigation: {
            nextEl: '.js-products-slider-next',
            prevEl: '.js-products-slider-prev',
        },
        pagination: {
            el: '.js-products-slider-pagination',
            clickable: true,
        },
        slidesPerView: 'auto',
        breakpoints: {
            // when window width is >= 320px
            320: {
                spaceBetween: 8,
                slidesPerView: 2,
            },
            // when window width is >= 480px
            480: {
                spaceBetween: 16,
                slidesPerView: 2,
            },
            // when window width is >= 768px
            768: {
                spaceBetween: 12,
                slidesPerView: 3,
            },
            // when window width is >= 992px
            992: {
                spaceBetween: 20,
                slidesPerView: 4,
            }
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Info Slider
    |--------------------------------------------------------------------------
    */

    let infoSlider = new Swiper('.js-info-slider', {
        speed: 600,
        mousewheel: false,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        effect: 'fade',
        autoHeight: true,
        fadeEffect: {
            crossFade: true
        },
        spaceBetween: 0,
        navigation: {
            nextEl: '.js-info-slider-next',
            prevEl: '.js-info-slider-prev',
        },
        pagination: {
            el: '.js-info-slider-pagination',
            clickable: true,
        },
        slidesPerView: 1,
    });

    /*
    |--------------------------------------------------------------------------
    | Notes Slider
    |--------------------------------------------------------------------------
    */

    function updNotesNumericPagination() {
        this.el.querySelector(".js-notes-counter").innerHTML =
            '<span class="notes-slider__counter-item">' +
            (this.realIndex + 1) +
            '</span> / <span class="notes-slider__counter-total">' +
            this.el.slidesQuantity +
            "</span>";
    }

    document.querySelectorAll(".js-notes-slider").forEach(function (node) {
        // Getting slides quantity before slider clones them
        node.slidesQuantity = node.querySelectorAll(".swiper-slide").length;

        // Swiper initialization
        new Swiper(node, {
            speed: 600,
            loop: true,
            mousewheel: false,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            autoHeight: true,
            spaceBetween: 40,
            navigation: {
                nextEl: '.js-notes-slider-next',
                prevEl: '.js-notes-slider-prev',
            },
            pagination: { el: node.querySelector(".js-notes-slider-pagination") },
            on: {
                // Secondary pagination is update after initialization and after slide change
                init: updNotesNumericPagination,
                slideChange: updNotesNumericPagination
            },
            slidesPerView: 4,
            breakpoints: {
                // when window width is >= 320px
                320: {
                    spaceBetween: 16,
                    freeMode: false,
                    slidesPerView: 1.3,
                },
                // when window width is >= 480px
                480: {
                    spaceBetween: 20,
                    slidesPerView: 2.3,
                },
                // when window width is >= 768px
                768: {
                    spaceBetween: 20,
                    slidesPerView: 3,
                },
                // when window width is >= 992px
                992: {
                    spaceBetween: 23,
                },
                // when window width is >= 1170px
                1170: {
                    spaceBetween: 40,
                    slidesPerView: 4,
                }
            }
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Media Slider
    |--------------------------------------------------------------------------
    */

    function updMediaNumericPagination() {
        $('.swiper-slide').each(function () {
            var youtubePlayer = $(this).find('iframe').get(0);
            if (youtubePlayer) {
                youtubePlayer.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
            }
        });
        this.el.querySelector(".js-media-counter").innerHTML =
            '<span class="media-slider__counter-item">' +
            (this.realIndex + 1) +
            '</span> / <span class="media-slider__counter-total">' +
            this.el.slidesQuantity +
            "</span>";
    }

    document.querySelectorAll(".js-media-slider").forEach(function (node) {
        // Getting slides quantity before slider clones them
        node.slidesQuantity = node.querySelectorAll(".swiper-slide").length;

        // Swiper initialization
        new Swiper(node, {
            speed: 900,
            loop: true,
            mousewheel: false,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            observer: true,
            observeParents: true,
            autoHeight: false,
            spaceBetween: 0,
            centeredSlides: true,
            navigation: {
                nextEl: '.js-media-slider-next',
                prevEl: '.js-media-slider-prev',
            },
            pagination: { el: node.querySelector(".js-media-slider-pagination") },
            on: {
                // Secondary pagination is update after initialization and after slide change
                init: updMediaNumericPagination,
                slideChange: updMediaNumericPagination,
            },
            slidesPerView: 1.5,
            breakpoints: {
                // when window width is >= 320px
                320: {
                    spaceBetween: 0,
                    slidesPerView: 1.13,
                },
                // when window width is >= 480px
                480: {
                    spaceBetween: 0,
                    slidesPerView: 1.2,
                },
                // when window width is >= 768px
                768: {
                    spaceBetween: 0,
                    slidesPerView: 1.2,
                },
                // when window width is >= 992px
                992: {
                    spaceBetween: 0,
                    slidesPerView: 1.8,
                },
                // when window width is >= 1170px
                1170: {
                    spaceBetween: 0,
                    slidesPerView: 1.8,
                },
                // when window width is >= 1360px
                1360: {
                    spaceBetween: 0,
                    slidesPerView: 1.77,
                },
                // when window width is >= 1360px
                1440: {
                    spaceBetween: 0,
                    slidesPerView: 1.77,
                }
            }
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Light Gallery
    |--------------------------------------------------------------------------
    */

    var lightGalleryItem = $(".js-lg-item").not('.swiper-slide-duplicate');

	$('.js-lg').lightGallery({
		selector: lightGalleryItem,
	});

    /*
    |--------------------------------------------------------------------------
    | Bootstrap Tooltip
    |--------------------------------------------------------------------------
    */

    $('[data-toggle="tooltip"]').tooltip();
    // colored tooltip
    $('[data-toggle="tooltip-primary"]').tooltip({
        template: '<div class="tooltip tooltip-primary" role="tooltip"><div class="arrow"><\/div><div class="tooltip-inner"><\/div><\/div>'
    });
    $('[data-toggle="tooltip-secondary"]').tooltip({
        template: '<div class="tooltip tooltip-secondary" role="tooltip"><div class="arrow"><\/div><div class="tooltip-inner"><\/div><\/div>'
    });

    /*
    |--------------------------------------------------------------------------
    | Clipboard.js
    |--------------------------------------------------------------------------
    */

    if ($('.clipboard-icon').length) {
        var clipboard = new ClipboardJS('.clipboard-icon');

        $('.clipboard-icon').attr('data-toggle', 'tooltip').attr('title', 'Copy to clipboard');


        $('[data-toggle="tooltip"]').tooltip();

        clipboard.on('success', function(e) {
            e.trigger.classList.value = 'clipboard-icon btn-current'
            $('.btn-current').tooltip('hide');
            e.trigger.dataset.originalTitle = 'Copied';
            $('.btn-current').tooltip('show');
            setTimeout(function(){
                $('.btn-current').tooltip('hide');
                e.trigger.dataset.originalTitle = 'Copy to clipboard';
                e.trigger.classList.value = 'clipboard-icon'
            },1000);
            e.clearSelection();
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Select2
    |--------------------------------------------------------------------------
    */

    $(".select2").each(function() {
        $(this).select2({
            placeholder: $(this).attr('data-placeholder'),
            width: '100%',
            minimumResultsForSearch: -1
        });
    });

    $(".select2-search").each(function() {
        $(this).select2({
            placeholder: $(this).attr('data-placeholder'),
            width: '100%',
            allowClear: false,
            minimumResultsForSearch: 5
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Amaze UI Datetime Picker
    | https://github.com/amazeui/datetimepicker
    |--------------------------------------------------------------------------
    */

    /**
     * Russian translation for bootstrap-datetimepicker
     * Victor Taranenko <darwin@snowdale.com>
     */
    ;(function($){
        $.fn.datetimepicker.dates['ru'] = {
            days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"],
            daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб", "Вск"],
            daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
            months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
            today: "Сегодня",
            suffix: [],
            meridiem: []
        };
    }(jQuery));

    // AmazeUI Datetimepicker
    $('.js-datetimepicker').datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        autoclose: true,
        language: 'ru'
    });

    /*
    |--------------------------------------------------------------------------
    | Back to Top
    |--------------------------------------------------------------------------
    */

    $(window).on("scroll", function(e) {
        if ($(this).scrollTop() > 0) {
            $('.js-back-to-top').fadeIn('slow');
        } else {
            $('.js-back-to-top').fadeOut('slow');
        }
    });

    $(".js-back-to-top").on("click", function(e) {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    /*
    |--------------------------------------------------------------------------
    | Bootstrap 4 - Close Popover When Losing Focus
    |--------------------------------------------------------------------------
    |
    | A JavaScript snippet that closes a Bootstrap 4 popover when clicking off
    | of it, but unlike the default behaviour, allows it to stay open when
    | clicking within the popover itself.
    */

    $(function(){

        $("[data-toggle=popover]").popover({
            html : true,
            trigger: 'hover click', //  close on click elsewhere
            content: function() {
                var content = $(this).attr("data-popover-content");
                return $(content).children(".popover-body").html();
            },
            title: function() {
                var title = $(this).attr("data-popover-content");
                return $(title).children(".popover-heading").html();
            }
        });

        $('body').on('click', function (e) {
            $('[data-toggle="popover"]').each(function () {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });

    });

});
